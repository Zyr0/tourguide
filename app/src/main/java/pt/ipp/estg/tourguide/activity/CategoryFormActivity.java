package pt.ipp.estg.tourguide.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import pt.ipp.estg.tourguide.R;
import pt.ipp.estg.tourguide.classes.Category;
import pt.ipp.estg.tourguide.others.DataBaseHelper;
import pt.ipp.estg.tourguide.others.TextWatcherHelper;

public class CategoryFormActivity extends AppCompatActivity {

    private TextInputEditText name, desc;
    private TextInputLayout layoutName, layoutDesc;
    private int catId;
    private DataBaseHelper helper;

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_form);
        helper = new DataBaseHelper(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar bar = getSupportActionBar();
        Category cat = null;
        catId = getIntent().getIntExtra("pos", -1);

        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
            if (catId != -1) {
                bar.setTitle(R.string.category_form_edit);
            } else {
                bar.setTitle(R.string.category_form_title);
            }
        }

        layoutName = findViewById(R.id.text_name_layout);
        layoutDesc = findViewById(R.id.text_desc_layout);
        name = findViewById(R.id.input_name);
        desc = findViewById(R.id.input_desc);

        if (catId != -1) {
            cat = helper.getCategory(catId, null);
            name.setText(cat.getNome());
            desc.setText(cat.getDesc());
        }

        layoutDesc.setCounterEnabled(true);
        layoutDesc.setCounterMaxLength(100);

        name.addTextChangedListener(new TextWatcherHelper(
                this, name, layoutName, TextWatcherHelper.NAME));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            case R.id.save_menu:
                submit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void submit() {
        DataBaseHelper helper = new DataBaseHelper(this);

        if (!layoutName.isErrorEnabled() && !layoutDesc.isErrorEnabled()) {
            if (name.getText() != null && desc.getText() != null) {
                int nameSize = name.getText().toString().length();
                if (nameSize != 0) {
                    Category c = null;
                    if (catId == -1) c = helper.getCategory(-1, name.getText().toString());
                    if (c == null) {
                        if (catId != -1) {
                            helper.updateCategory(catId, name.getText().toString(), desc.getText().toString());
                        } else {
                            helper.createCategory(name.getText().toString(), desc.getText().toString());
                        }
                        Intent intent = new Intent(this, MainActivity.class);
                        intent.putExtra("targetTab", this.getClass().getSimpleName());
                        startActivity(intent);
                        finish();
                    } else {
                        showSnack(R.string.name_already_used);
                    }
                } else {
                    showSnack(R.string.err_name_invalid);
                }
            }
        } else {
            showSnack(R.string.err_invalid);
        }
    }

    private void showSnack(int id) {
        hideKeyboardFrom(getBaseContext(), getCurrentFocus().getRootView());
        Snackbar.make(findViewById(R.id.coordinator), id, Snackbar.LENGTH_SHORT)
                .show();
    }
}
