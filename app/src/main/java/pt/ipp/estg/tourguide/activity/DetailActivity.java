package pt.ipp.estg.tourguide.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import pt.ipp.estg.tourguide.R;
import pt.ipp.estg.tourguide.classes.Place;
import pt.ipp.estg.tourguide.fragment.CategoryDetailsFragment;
import pt.ipp.estg.tourguide.fragment.MapFragment;
import pt.ipp.estg.tourguide.fragment.PlaceDetailsFragment;
import pt.ipp.estg.tourguide.fragment.RouteDetailsFragment;

public class DetailActivity extends AppCompatActivity implements
        PlaceDetailsFragment.Listener, RouteDetailsFragment.Listener, CategoryDetailsFragment.Listener {

    private boolean isMap;
    private int placePos, catPos, routePos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        placePos = getIntent().getIntExtra("placePos", -1);
        catPos = getIntent().getIntExtra("catPos", -1);
        routePos = getIntent().getIntExtra("routePos", -1);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
            String title;
            if (catPos > -1) {
                title = getString(R.string.category_details);
            } else if (routePos > -1) {
                title = getString(R.string.route_details);
            } else {
                title = getString(R.string.location_details);
            }
            bar.setTitle(title);
        }

        if (savedInstanceState != null) {
            isMap = savedInstanceState.getBoolean("map");
            return;
        } else {
            isMap = false;
        }

        Fragment frag;

        if (catPos > -1) {
            frag = new CategoryDetailsFragment();
        } else if (routePos > -1) {
            frag = new RouteDetailsFragment();
        } else {
            frag = new PlaceDetailsFragment();
        }

        if (findViewById(R.id.phone) == null) {
            if (catPos > -1) {
                findViewById(R.id.map).setVisibility(View.GONE);
            } else {
                Fragment map = new MapFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                transaction.add(R.id.map, map);
                transaction.commit();
            }
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.add(R.id.frame, frag);
        transaction.commit();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("map", isMap);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        boolean fromWidget =
                getIntent().getBooleanExtra("fromWidget", false);
        if (fromWidget) {
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                boolean fromWidget =
                        getIntent().getBooleanExtra("fromWidget", false);
                if (fromWidget) {
                    Intent i = new Intent(this, MainActivity.class);
                    startActivity(i);
                }
                super.onBackPressed();
                break;
            case R.id.edit_menu:
                Intent intent;
                if (catPos > -1) {
                    intent = new Intent(this, CategoryFormActivity.class);
                    intent.putExtra("pos", catPos);
                } else if (routePos > -1) {
                    intent = new Intent(this, RouteFormActivity.class);
                    intent.putExtra("pos", routePos);
                } else {
                    intent = new Intent(this, PlaceFormActivity.class);
                    intent.putExtra("pos", placePos);
                }

                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPlaceClick(View v, Place place) {
        MapFragment frag;
        if (findViewById(R.id.phone) != null) {
            isMap = true;
        }
        frag = new MapFragment();
        frag.setMarker(place);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.replace(R.id.frame, frag);
        if (isMap) {
            transaction.addToBackStack("REEEEE");
        }
        transaction.commit();
    }

    @Override
    public int setPlacePos() {
        return placePos;
    }

    @Override
    public void onRouteClick(View v, List<Place> list) {
        MapFragment frag;

        if (findViewById(R.id.phone) != null) {
            isMap = true;
        }
        frag = new MapFragment();
        frag.setRoute(list, routePos);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.replace(R.id.frame, frag);
        if (isMap) {
            transaction.addToBackStack("REEEEE");
        }
        transaction.commit();
    }

    @Override
    public int setRoutePos() {
        return routePos;
    }

    @Override
    public int setCatPos() {
        return catPos;
    }
}
