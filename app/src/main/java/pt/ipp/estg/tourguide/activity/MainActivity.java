package pt.ipp.estg.tourguide.activity;

import android.Manifest;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import pt.ipp.estg.tourguide.R;
import pt.ipp.estg.tourguide.fragment.CategoryListFragment;
import pt.ipp.estg.tourguide.fragment.PlacesListFragment;
import pt.ipp.estg.tourguide.fragment.RoutesListFragment;
import pt.ipp.estg.tourguide.preferences.PreferencesActivity;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private static final int PERMISSION_REQUEST_CODE = 100;

    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar bar = getSupportActionBar();

        if (bar != null) {
            bar.setIcon(R.drawable.ic_car);
        }

        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getSupportFragmentManager());

        ViewPager pager = findViewById(R.id.pager);
        pager.setAdapter(adapter);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                YoYo.with(Techniques.FadeIn).duration(990).playOn(findViewById(R.id.fab));
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        if (getIntent().getExtras() != null) {
            String targetTab = getIntent().getStringExtra("targetTab");
            if (targetTab != null) {
                int tab;
                switch (targetTab) {
                    case "CategoryFormActivity":
                        tab = 1;
                        break;
                    case "RouteFormActivity":
                        tab = 2;
                        break;
                    default:
                        tab = 0;
                        break;
                }
                pager.setCurrentItem(tab, true);
            }
        }

        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(pager);
        findViewById(R.id.fab).setOnClickListener(v -> {
            switch (tabLayout.getSelectedTabPosition()) {
                case 0:
                    startActivity(new Intent(this, PlaceFormActivity.class));
                    break;
                case 1:
                    startActivity(new Intent(this, CategoryFormActivity.class));
                    break;
                case 2:
                    startActivity(new Intent(this, RouteFormActivity.class));
                    break;
            }
        });

        /*______________________

        Bundle bundle = new Bundle();
        bundle.putDouble("myLat", lastKnownLocation.getLatitude());
        bundle.putDouble("myLng", lastKnownLocation.getLongitude());

        Log.d("log", "LAT: "+bundle.getDouble("myLat")+"Lng: "+bundle.getDouble("myLng"));
        PlacesListFragment frag = new PlacesListFragment();
        frag.setArguments(bundle);
     ______*/

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setQueryHint(getString(R.string.search_hint));
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_pref:
                startActivity(new Intent(this, PreferencesActivity.class));
                return true;
            case R.id.btn_all_map:
                startActivity(new Intent(this, MapPlacesActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        Intent i = new Intent(this, ResultSearchActivity.class);
        i.putExtra("s", s);
        startActivity(i);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        int fineLocation = ActivityCompat
                .checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int readStorage = ActivityCompat
                .checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeStorage = ActivityCompat
                .checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (fineLocation != PackageManager.PERMISSION_GRANTED ||
                readStorage != PackageManager.PERMISSION_GRANTED ||
                writeStorage != PackageManager.PERMISSION_GRANTED) {
            String[] permissions = new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE};
            ActivityCompat.requestPermissions(MainActivity.this, permissions,
                    PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    return new PlacesListFragment();
                case 1:
                    return new CategoryListFragment();
                case 2:
                    return new RoutesListFragment();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.places_tab);
                case 1:
                    return getResources().getString(R.string.cat_tab);
                case 2:
                    return getResources().getString(R.string.routes_tab);
                default:
                    return null;
            }
        }
    }
}
