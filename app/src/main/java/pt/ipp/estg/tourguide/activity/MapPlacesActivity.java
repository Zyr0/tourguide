package pt.ipp.estg.tourguide.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import pt.ipp.estg.tourguide.R;
import pt.ipp.estg.tourguide.classes.Place;
import pt.ipp.estg.tourguide.fragment.MapFragment;
import pt.ipp.estg.tourguide.others.DataBaseHelper;
import pt.ipp.estg.tourguide.services.LocationService;

public class MapPlacesActivity extends AppCompatActivity {

    private LocationService locationService;
    private boolean isServiceBound;
    private int radius;

    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isServiceBound = false;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocationService.MyBinder myBinder = (LocationService.MyBinder) service;
            locationService = myBinder.getService();
            isServiceBound = true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_places);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar bar = getSupportActionBar();

        Intent intent = new Intent(this, LocationService.class);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);

        List<Place> list = new ArrayList<>();

        DataBaseHelper helper = new DataBaseHelper(this);
        helper.getPlaces(list, "rating", false);

        String prefRadius = PreferenceManager.getDefaultSharedPreferences(this)
                .getString("search_radius", "5");
        radius = 5;
        if (prefRadius != null) {
            radius = Integer.valueOf(prefRadius);
        }

        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
            String title = String.valueOf(radius) + " " + getString(R.string.km_radius);
            bar.setTitle(title);
        }

        Button show = findViewById(R.id.show_all);

        show.setOnClickListener(v -> {
            LatLng latLng = locationService.getCoordinates();
            Place.closestPlaces(list, latLng.latitude, latLng.longitude);

            int size = list.size();
            List<Place> placeHolder = new ArrayList<>(list);
            for (int i = 0; i < size; i++) {
                Place p = placeHolder.get(i);
                if (p._distance > radius) {
                    list.remove(p);
                }
            }

            MapFragment frag;
            frag = new MapFragment();
            frag.setMarkers(list);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            transaction.replace(R.id.frame, frag);
            transaction.commit();

            String text = getString(R.string.show_all_places_in_a) + " " +
                    String.valueOf(radius) + " " + getString(R.string.km_radius);
            Toast.makeText(this, text, Toast.LENGTH_LONG).show();

            show.setVisibility(View.GONE);
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (isServiceBound) {
            unbindService(mServiceConnection);
            isServiceBound = false;
        }
    }
}
