package pt.ipp.estg.tourguide.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import pt.ipp.estg.tourguide.R;
import pt.ipp.estg.tourguide.classes.Category;
import pt.ipp.estg.tourguide.classes.Place;
import pt.ipp.estg.tourguide.others.DataBaseHelper;
import pt.ipp.estg.tourguide.others.TextWatcherHelper;

public class PlaceFormActivity extends AppCompatActivity {

    private static final int PLACE_PICKER_REQUEST = 1;
    private static final int PICK_IMAGE_MULTIPLE = 2;

    private TextInputEditText name, desc, lat, lng;
    private TextInputLayout layoutName, layoutDesc, layoutLat, layoutLng;
    private RatingBar rating;
    private int rateValue = 0, placeId;
    private DataBaseHelper helper;
    private boolean[] SelectedBoolean;
    private String[] AlertDialogItems;
    private List<String> categoriesSelected, imagesEncoded;
    private List<Category> categories;

    public static void hideKeyboardFrom(Context context, View view) {
        view = view.getRootView();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_form);
        helper = new DataBaseHelper(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar bar = getSupportActionBar();

        placeId = getIntent().getIntExtra("pos", -1);
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
            if (placeId != -1) {
                bar.setTitle(R.string.place_form_edit);
            } else {
                bar.setTitle(R.string.place_form_title);
            }
        }

        setUpViews();

        categoriesSelected = new ArrayList<>();
        categories = new ArrayList<>();

        List<Category> placeCategories = new ArrayList<>();

        if (placeId != -1) {
            Place local = helper.getPlace(placeId);
            name.setText(local.getNome());
            desc.setText(local.getDesc());
            lat.setText(String.valueOf(local.getLocation().latitude));
            lng.setText(String.valueOf(local.getLocation().longitude));
            rateValue = Integer.valueOf(local.getRating());
            rating.setRating((float) rateValue);
            imagesEncoded = local.getPhotos();
            helper.getCategoriesByPlace(placeCategories, placeId);
        }

        helper.getCategories(categories);

        AlertDialogItems = new String[categories.size()];
        SelectedBoolean = new boolean[categories.size()];

        int sizeCategories = categories.size();
        int sizeSelected = placeCategories.size();

        for (int i = 0; i < sizeCategories; i++) {
            AlertDialogItems[i] = categories.get(i).getNome();
            if (placeId != -1) {
                for (int j = 0; j < sizeSelected; j++) {
                    if (placeCategories.get(j).getId() == categories.get(i).getId()) {
                        SelectedBoolean[i] = true;
                        categoriesSelected.add(placeCategories.get(j).getNome());
                    }
                }
            }
        }

        setUpListeners();
    }

    private void setUpViews() {
        layoutName = findViewById(R.id.text_name_layout);
        layoutDesc = findViewById(R.id.text_desc_layout);
        layoutLat = findViewById(R.id.text_lat_layout);
        layoutLng = findViewById(R.id.text_lng_layout);

        name = findViewById(R.id.input_name);
        desc = findViewById(R.id.input_desc);
        lat = findViewById(R.id.input_lat);
        lng = findViewById(R.id.input_lng);
        rating = findViewById(R.id.rating);

        layoutDesc.setCounterEnabled(true);
        layoutDesc.setCounterMaxLength(100);
    }

    private void setUpListeners() {
        name.addTextChangedListener(new TextWatcherHelper(
                this, name, layoutName, TextWatcherHelper.NAME));
        lat.addTextChangedListener(new TextWatcherHelper(
                this, lat, layoutLat, TextWatcherHelper.LAT));
        lng.addTextChangedListener(new TextWatcherHelper(
                this, lng, layoutLng, TextWatcherHelper.LNG));

        rating.setOnRatingBarChangeListener(
                (ratingBar, rating1, fromUser) -> this.rateValue = (int) rating.getRating());

        findViewById(R.id.location_btn).setOnClickListener(v -> {
            try {
                PlacePicker.IntentBuilder intentBuilder =
                        new PlacePicker.IntentBuilder();
                Intent intent = intentBuilder.build(this);
                startActivityForResult(intent, PLACE_PICKER_REQUEST);

            } catch (GooglePlayServicesRepairableException
                    | GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }
        });

        findViewById(R.id.btn_images).setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            CharSequence title = getString(R.string.select_pics);
            startActivityForResult(
                    Intent.createChooser(intent, title), PICK_IMAGE_MULTIPLE);
            hideKeyboardFrom(getBaseContext(), getCurrentFocus());
        });

        findViewById(R.id.btn_cat).setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.cat_tab);

            categoriesSelected.clear();
            builder.setMultiChoiceItems(
                    AlertDialogItems,
                    SelectedBoolean, (dialog, which, isChecked) -> {
                    });

            builder.setPositiveButton("OK", (dialog, which) -> {
                int a = 0;
                while (a < SelectedBoolean.length) {
                    boolean value = SelectedBoolean[a];
                    if (value) {
                        categoriesSelected.add(categories.get(a).getNome());
                    }

                    a++;
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        });
    }

    private void handleImages(Intent data) {

        imagesEncoded = new ArrayList<>();
        if (data.getClipData() != null) {
            int count = data.getClipData().getItemCount();
            ClipData clipData = data.getClipData();

            for (int i = 0; i < count; i++) {
                ClipData.Item item = clipData.getItemAt(i);
                Uri uri = item.getUri();
                imagesEncoded.add(getRealPath(uri));
            }
        } else if (data.getData() != null) {
            Uri uri = data.getData();
            imagesEncoded.add(getRealPath(uri));
        }
        TextView images = findViewById(R.id.images_names);
        images.setVisibility(View.VISIBLE);
        images.setText(R.string.images);

        List<String> imagesFinalPath = new ArrayList<>();
        int size = imagesEncoded.size();
        for (int i = 0; i < size; i++) {
            images.append("\n");
            String path = imagesEncoded.get(i);
            Log.d("log", "handleImages: "+ path);
            String[] imageNameSeparated = path.split("/");
            String name = imageNameSeparated[imageNameSeparated.length - 1];
            Log.d("log", "handleImages: " + name);
            images.append(name);
            try {
                imagesFinalPath.add(copyFile(path, name));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        imagesEncoded = imagesFinalPath;
    }

    private String getRealPath(Uri uri){
        String filePath = "";
        String wholeId = DocumentsContract.getDocumentId(uri);

        String id = wholeId.split(":")[1];
        String[] column = { MediaStore.Images.Media.DATA };
        String sel = MediaStore.Images.Media._ID + " = ?";

        Cursor cursor = getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{id}, null);
        if (cursor != null) {
            int columnIndex = cursor.getColumnIndex(column[0]);
            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        }
        return filePath;
    }

    private String copyFile(String srcPath, String name) throws IOException {
        File sourceFile = new File(srcPath);
        String destPath = Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/TourGuide/" + name;
        File destFile = new File(destPath);
        if (!destFile.getParentFile().exists()) {
            if (!destFile.getParentFile().mkdirs()) {
                showSnack(R.string.err_creating_directory);
            }
        }

        if (!destFile.exists()) {
            if (!destFile.createNewFile()) {
                showSnack(R.string.err_creating_file);
                return srcPath;
            }
        } else {
            return destPath;
        }

        try (FileChannel source = new FileInputStream(sourceFile).getChannel();
             FileChannel destination = new FileOutputStream(destFile).getChannel()) {
            destination.transferFrom(source, 0, source.size());
            return destPath;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST
                && resultCode == Activity.RESULT_OK && data != null) {

            final com.google.android.gms.location.places.Place place = PlacePicker.getPlace(this, data);
            final CharSequence mapLat = String.valueOf(place.getLatLng().latitude);
            final CharSequence mapLng = String.valueOf(place.getLatLng().longitude);
            final CharSequence mapName = place.getName();
            final CharSequence mapDesc = place.getAddress();

            if (name.length() == 0) {
                name.setText(mapName);
            }
            if (desc.length() == 0) {
                desc.setText(mapDesc);
                if (place.getPhoneNumber() != null) {
                    desc.append("\n");
                    desc.append(place.getPhoneNumber());
                }
            }

            lat.setText(mapLat);
            lng.setText(mapLng);

        } else if (requestCode == PICK_IMAGE_MULTIPLE
                && resultCode == Activity.RESULT_OK && data != null) {
            handleImages(data);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            case R.id.save_menu:
                submit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void submit() {
        if (name.getText() != null && desc.getText() != null &&
                lat.getText() != null && lng.getText() != null &&
                lat.getText().length() > 0 && lng.getText().length() > 0) {

            if (!layoutName.isErrorEnabled() && !layoutDesc.isErrorEnabled() &&
                    !layoutLng.isErrorEnabled() && !layoutLat.isErrorEnabled()
                    && rateValue > 0 && categoriesSelected.size() > 0 && imagesEncoded != null) {

                double doubleLat = Double.valueOf(lat.getText().toString());
                double doubleLng = Double.valueOf(lng.getText().toString());

                if (name.getText().toString().length() != 0) {
                    if (placeId == -1) {
                        helper.createPlace(name.getText().toString(), desc.getText().toString(),
                                rateValue, new LatLng(doubleLat, doubleLng),
                                categoriesSelected, imagesEncoded);
                    } else {
                        helper.updatePlace(placeId, name.getText().toString(), desc.getText().toString(),
                                rateValue, new LatLng(doubleLat, doubleLng),
                                categoriesSelected, imagesEncoded);
                    }
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.putExtra("targetTab", this.getClass().getSimpleName());
                    startActivity(intent);
                } else {
                    showSnack(R.string.err_name_invalid);
                }
            } else {
                if (name.getText().toString().length() == 0 || layoutName.isErrorEnabled()) {
                    showSnack(R.string.err_name_invalid);
                } else if (lat.getText().toString().length() == 0 ||
                        lat.getText().toString().length() == 0 ||
                        layoutLng.isErrorEnabled() ||
                        layoutLat.isErrorEnabled()) {
                    showSnack(R.string.err_coordinates_required);
                } else if (rateValue == 0) {
                    showSnack(R.string.err_rating_required);
                } else if (categoriesSelected.size() <= 0) {
                    showSnack(R.string.err_category_selected);
                } else if (imagesEncoded == null) {
                    showSnack(R.string.err_images);
                }
            }
        } else {
            showSnack(R.string.no_coord);
        }
        sendBroadcast(new Intent(getString(R.string.action_widget_update)));
    }

    private void showSnack(int id) {
        hideKeyboardFrom(getBaseContext(), getCurrentFocus());
        Snackbar.make(findViewById(R.id.coordinator), id, Snackbar.LENGTH_SHORT)
                .show();
    }
}
