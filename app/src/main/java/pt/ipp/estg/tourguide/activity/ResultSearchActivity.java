package pt.ipp.estg.tourguide.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pt.ipp.estg.tourguide.R;
import pt.ipp.estg.tourguide.classes.Place;
import pt.ipp.estg.tourguide.others.DataBaseHelper;
import pt.ipp.estg.tourguide.others.ListAdapter;

public class ResultSearchActivity extends AppCompatActivity implements ListAdapter.Listener {

    private List<Place> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_search);

        String by = getIntent().getStringExtra("s");
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
            String title = getString(R.string.search) + " by " + by;
            bar.setTitle(title);
        }

        DataBaseHelper helper = new DataBaseHelper(this);
        list = new ArrayList<>();

        helper.searchByCategory(list, by, false);
        if (list.size() > 0) {
            ListAdapter adapter = new ListAdapter(list, ListAdapter.PLACES);
            adapter.setListener(this);

            RecyclerView recyclerView = findViewById(R.id.recycler_view);
            recyclerView.setAdapter(adapter);

            LinearLayoutManager manager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(manager);
        } else {
            TextView nothing = findViewById(R.id.nothing_show);
            nothing.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(int pos) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("placePos", list.get(pos).getId());
        startActivity(intent);
    }

    @Override
    public void longClick(int pos) {

    }
}
