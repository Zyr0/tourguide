package pt.ipp.estg.tourguide.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.util.ArrayList;
import java.util.List;

import pt.ipp.estg.tourguide.R;
import pt.ipp.estg.tourguide.classes.Place;
import pt.ipp.estg.tourguide.classes.Route;
import pt.ipp.estg.tourguide.others.DataBaseHelper;
import pt.ipp.estg.tourguide.others.TextWatcherHelper;

public class RouteFormActivity extends AppCompatActivity {

    private TextInputEditText name, desc;
    private TextInputLayout layoutName, layoutDesc;
    private List<Integer> placeSelected;
    private boolean[] SelectedBoolean;
    private DataBaseHelper helper;
    private int routeId;

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_form);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        routeId = getIntent().getIntExtra("pos", -1);

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
            if (routeId > -1) {
                bar.setTitle(R.string.route_form_title_edit);
            } else {
                bar.setTitle(R.string.route_form_title);
            }
        }

        helper = new DataBaseHelper(this);
        setUpViews();

        List<Place> alreadySelected = new ArrayList<>();

        if (routeId > -1) {
            Route r = helper.getRoute(routeId);
            name.setText(r.getNome());
            desc.setText(r.getDesc());
            alreadySelected = r.getPlaces();
        }

        List<Place> places = new ArrayList<>();
        helper.getPlaces(places, "rating", false);

        placeSelected = new ArrayList<>();
        SelectedBoolean = new boolean[places.size()];
        String[] AlertDialogItems = new String[places.size()];

        int size = places.size();
        int alreadySize = alreadySelected.size();
        for (int i = 0; i < size; i++) {
            AlertDialogItems[i] = places.get(i).getNome();
            if (routeId > -1) {
                for (int j = 0; j < alreadySize; j++) {
                    if (places.get(i).getId() == alreadySelected.get(j).getId()) {
                        SelectedBoolean[i] = true;
                        placeSelected.add(places.get(i).getId());
                    }
                }
            }
        }

        findViewById(R.id.route_path).setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.places_tab);

            placeSelected.clear();
            builder.setMultiChoiceItems(
                    AlertDialogItems,
                    SelectedBoolean, (dialog, which, isChecked) -> {
                    });
            builder.setPositiveButton("OK", (dialog, which) -> {
                int a = 0;
                while (a < SelectedBoolean.length) {
                    boolean value = SelectedBoolean[a];

                    if (value) {
                        placeSelected.add(places.get(a).getId());
                    }

                    a++;
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        });
    }

    private void setUpViews() {
        layoutName = findViewById(R.id.text_name_layout);
        layoutDesc = findViewById(R.id.text_desc_layout);
        name = findViewById(R.id.input_name);
        desc = findViewById(R.id.input_desc);

        layoutDesc.setCounterEnabled(true);
        layoutDesc.setCounterMaxLength(100);

        name.addTextChangedListener(new TextWatcherHelper(
                this, name, layoutName, TextWatcherHelper.NAME));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            case R.id.save_menu:
                submit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void submit() {

        if (!layoutName.isErrorEnabled() && !layoutDesc.isErrorEnabled()) {
            if (name.getText() != null && desc.getText() != null && placeSelected.size() > 1) {
                int nameSize = name.getText().toString().length();
                if (nameSize != 0) {
                    if (routeId > -1) {
                        helper.updateRoute(routeId, name.getText().toString(),
                                desc.getText().toString(), placeSelected);
                    } else {
                        helper.createRoute(name.getText().toString(),
                                desc.getText().toString(), placeSelected);
                    }
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.putExtra("targetTab", this.getClass().getSimpleName());
                    startActivity(intent);
                } else {
                    showSnack(R.string.err_name_invalid);
                }
            } else {
                showSnack(R.string.err_place_short);
            }
        } else {
            showSnack(R.string.err_invalid);
        }
    }

    private void showSnack(int id) {
        hideKeyboardFrom(getBaseContext(), getCurrentFocus().getRootView());
        Snackbar.make(findViewById(R.id.coordinator), id, Snackbar.LENGTH_SHORT)
                .show();
    }
}
