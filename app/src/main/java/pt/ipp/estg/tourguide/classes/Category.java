package pt.ipp.estg.tourguide.classes;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import static pt.ipp.estg.tourguide.others.DataBaseHelper.CATLOC_FIRST_COL;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.CATLOC_SECOND_COL;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.CATLOC_TABLE_NAME;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.CAT_FIRST_COL;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.CAT_SECOND_COL;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.CAT_TABLE_NAME;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.CAT_THIRD_COL;

public class Category {
    private int id;
    private String nome;
    private String desc;

    public Category(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Category(int id, String nome, String desc) {
        this.id = id;
        this.nome = nome;
        this.desc = desc;
    }

    public static void createCategory(SQLiteDatabase db,
                                      final String name, final String description) {
        ContentValues values = new ContentValues();
        values.put(CAT_SECOND_COL, name);
        values.put(CAT_THIRD_COL, description);
        db.insert(CAT_TABLE_NAME, null, values);
    }

    public static void updateCategory(SQLiteDatabase db,
                                      final int id, final String name, final String desc) {
        ContentValues values = new ContentValues();
        values.put(CAT_SECOND_COL, name);
        values.put(CAT_THIRD_COL, desc);
        String where = CAT_FIRST_COL + " = ?";
        String[] args = {String.valueOf(id)};
        db.update(CAT_TABLE_NAME, values, where, args);
    }

    public static void getCategories(SQLiteDatabase db, List<Category> list, String by,
                                     boolean exact) {

        String selection = null;
        String[] selectionArgs = null;
        if (by != null) {
            if (exact) {
                selection = CAT_SECOND_COL + " = ?";
                selectionArgs = new String[]{by};
            } else {
                selection = CAT_SECOND_COL + " LIKE ?";
                selectionArgs = new String[]{"%" + by + "%"};
            }
        }

        Cursor c = db.query(CAT_TABLE_NAME,
                null,
                selection,
                selectionArgs,
                null,
                null,
                CAT_FIRST_COL);

        if (c != null && c.moveToFirst()) {
            do {
                int id = c.getInt(c.getColumnIndex(CAT_FIRST_COL));
                String name = c.getString(c.getColumnIndex(CAT_SECOND_COL));
                String desc = c.getString(c.getColumnIndex(CAT_THIRD_COL));
                Category cat = new Category(id, name, desc);
                list.add(cat);
            } while (c.moveToNext());
            c.close();
        }

    }

    public static Category getCategory(SQLiteDatabase db, final int pos, final String nome) {
        String selection = CAT_FIRST_COL + " = ? ";
        String[] selectionArgs = {String.valueOf(pos)};
        String order = CAT_FIRST_COL;

        if (nome != null) {
            selection = CAT_SECOND_COL + " = ? ";
            selectionArgs = new String[]{nome};
            order = CAT_SECOND_COL;
        }

        Cursor cursor = db.query(
                CAT_TABLE_NAME,
                null,
                selection,
                selectionArgs,
                null,
                null,
                order);

        if (cursor != null && cursor.moveToFirst()) {
            Category category;

            do {
                int id = cursor.getInt(cursor.getColumnIndex(CAT_FIRST_COL));
                String name = cursor.getString(cursor.getColumnIndex(CAT_SECOND_COL));
                String desc = cursor.getString(cursor.getColumnIndex(CAT_THIRD_COL));
                category = new Category(id, name, desc);
            } while (cursor.moveToNext());

            cursor.close();

            return category;
        }
        return null;
    }

    public static void deleteCategory(SQLiteDatabase db, final int id) {
        String selection = CAT_FIRST_COL + " = ?";
        String[] selectionArgs = {String.valueOf(id)};
        db.delete(CAT_TABLE_NAME, selection, selectionArgs);
        String catlocSelection = CATLOC_SECOND_COL + " = ?";
        db.delete(CATLOC_TABLE_NAME, catlocSelection, selectionArgs);

    }

    public static void getCategoryByPlace(SQLiteDatabase db, List<Category> list, final int id_place) {
        Cursor c = db.query(CATLOC_TABLE_NAME,
                null,
                CATLOC_FIRST_COL + "= ?",
                new String[]{String.valueOf(id_place)},
                null,
                null,
                CATLOC_SECOND_COL);
        if (c != null && c.moveToFirst()) {
            do {
                int id = c.getInt(c.getColumnIndex(CATLOC_SECOND_COL));
                Category cat = getCategory(db, id, null);
                list.add(cat);
            } while (c.moveToNext());
            c.close();
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
