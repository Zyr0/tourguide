package pt.ipp.estg.tourguide.classes;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static pt.ipp.estg.tourguide.others.DataBaseHelper.CAMINHO_SECOND_COL;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.CAMINHO_TABLE_NAME;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.CATLOC_FIRST_COL;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.CATLOC_SECOND_COL;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.CATLOC_TABLE_NAME;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.CAT_FIRST_COL;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.CAT_SECOND_COL;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.CAT_TABLE_NAME;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.LOCAL_FIFTH_COL;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.LOCAL_FIRST_COL;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.LOCAL_FORTH_COL;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.LOCAL_SECOND_COL;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.LOCAL_SIXTH_COL;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.LOCAL_TABLE_NAME;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.LOCAL_THIRD_COL;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.PHOTO_FIRST_COL;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.PHOTO_SECOND_COL;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.PHOTO_TABLE_NAME;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.PHOTO_THIRD_COL;

public class Place {
    public double _distance;
    private int id;
    private String nome;
    private String desc;
    private String rating;
    private LatLng latLng;
    private List<Category> categories;
    private List<String> photos;

    public Place(int id, String nome, List<String> photos, LatLng latLng) {
        this.id = id;
        this.nome = nome;
        this.photos = photos;
        this.latLng = latLng;
    }

    public Place(int id, String nome, String desc, String rating, LatLng latLng,
                 List<Category> categories, List<String> photos) {
        this.id = id;
        this.nome = nome;
        this.desc = desc;
        this.rating = rating;
        this.latLng = latLng;
        this.categories = categories;
        this.photos = photos;
    }

    public static void createPlace(SQLiteDatabase db, final String name,
                                   final String description, final int rating, final LatLng latLng,
                                   final List<String> categories, final List<String> images) {
        ContentValues values = new ContentValues();
        values.put(LOCAL_SECOND_COL, name);
        values.put(LOCAL_THIRD_COL, description);
        values.put(LOCAL_FORTH_COL, rating);
        values.put(LOCAL_FIFTH_COL, latLng.latitude);
        values.put(LOCAL_SIXTH_COL, latLng.longitude);

        db.insert(LOCAL_TABLE_NAME, null, values);

        String[] localColumns = {LOCAL_FIRST_COL};
        String localSelection = LOCAL_FIFTH_COL + " = ? AND " + LOCAL_SIXTH_COL + " = ? ";
        String[] localSelectionArgs = {
                String.valueOf(latLng.latitude),
                String.valueOf(latLng.longitude)
        };

        Cursor c = db.query(
                LOCAL_TABLE_NAME,
                localColumns,
                localSelection,
                localSelectionArgs,
                null,
                null,
                LOCAL_FIRST_COL);

        int idLocal = -1;
        if (c != null && c.moveToNext()) {
            do {
                idLocal = c.getInt(c.getColumnIndex(LOCAL_FIRST_COL));
            } while (c.moveToNext());
            c.close();
        }

        ContentValues photoValues = new ContentValues();
        int imgCount = images.size();
        for (int i = 0; i < imgCount; i++) {
            photoValues.put(PHOTO_SECOND_COL, images.get(i));
            photoValues.put(PHOTO_THIRD_COL, idLocal);
            db.insert(PHOTO_TABLE_NAME, null, photoValues);
        }

        ContentValues catValues = new ContentValues();
        int catCount = categories.size();
        for (int i = 0; i < catCount; i++) {
            String selection = CAT_SECOND_COL + " = ?";
            String[] selectionArgs = {categories.get(i)};

            Cursor cursor = db.query(
                    CAT_TABLE_NAME,
                    null,
                    selection,
                    selectionArgs,
                    null,
                    null,
                    CAT_FIRST_COL);

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    int idCat = cursor.getInt(cursor.getColumnIndex(CAT_FIRST_COL));
                    catValues.put(CATLOC_SECOND_COL, idCat);
                    catValues.put(CATLOC_FIRST_COL, idLocal);
                    db.insert(CATLOC_TABLE_NAME, null, catValues);
                } while (cursor.moveToNext());

                cursor.close();
            }
        }
    }

    public static void getPlaces(SQLiteDatabase db, List<Place> list, final String by,
                                 final boolean search) {
        list.clear();
        String order;
        if (by != null) {
            switch (by) {
                case "rating":
                    order = LOCAL_FORTH_COL + " DESC";
                    break;
                case "a-z":
                    order = LOCAL_SECOND_COL + " ASC";
                    break;
                case "z-a":
                    order = LOCAL_SECOND_COL + " DESC";
                    break;
                case "recent":
                    order = LOCAL_FIRST_COL + " DESC";
                    break;
                default:
                    order = LOCAL_FIRST_COL + " DESC";
                    break;
            }
        } else {
            order = LOCAL_FIRST_COL + " DESC";
        }

        String selection = null;
        String[] selectionArgs = null;

        if (search) {
            selection = LOCAL_SECOND_COL + " LIKE ?";
            selectionArgs = new String[]{"%" + by + "%"};
        }

        Cursor localCursor = db.query(
                LOCAL_TABLE_NAME,
                null,
                selection,
                selectionArgs,
                null,
                null,
                order);

        if (localCursor != null && localCursor.moveToFirst()) {
            do {
                int idPlace = localCursor.getInt(localCursor.getColumnIndex(LOCAL_FIRST_COL));
                Place p = Place.getPlace(db, idPlace);
                list.add(p);
            } while (localCursor.moveToNext());
            localCursor.close();
        }
    }

    public static Place getPlace(SQLiteDatabase db, final int id_place) {

        String localSelection = LOCAL_FIRST_COL + " = ? ";
        String[] localSelectionArgs = {String.valueOf(id_place)};

        Cursor cursor = db.query(
                LOCAL_TABLE_NAME,
                null,
                localSelection,
                localSelectionArgs,
                null,
                null,
                LOCAL_FIRST_COL);

        int idPlace = -1;
        String name = "";
        String desc = "";
        String rating = "";
        LatLng location = null;

        if (cursor != null && cursor.moveToFirst()) {
            idPlace = cursor.getInt(cursor.getColumnIndex(LOCAL_FIRST_COL));
            name = cursor.getString(cursor.getColumnIndex(LOCAL_SECOND_COL));
            desc = cursor.getString(cursor.getColumnIndex(LOCAL_THIRD_COL));
            rating = cursor.getString(cursor.getColumnIndex(LOCAL_FORTH_COL));
            location = new LatLng(
                    cursor.getDouble(cursor.getColumnIndex(LOCAL_FIFTH_COL)),
                    cursor.getDouble(cursor.getColumnIndex(LOCAL_SIXTH_COL)));
            cursor.close();
        }

        String photoSelection = PHOTO_THIRD_COL + " = ? ";
        String[] photoSelectionArgs = {String.valueOf(idPlace)};

        Cursor photoCursor = db.query(
                PHOTO_TABLE_NAME,
                null,
                photoSelection,
                photoSelectionArgs,
                null,
                null,
                PHOTO_FIRST_COL);

        List<String> photos = new ArrayList<>();

        if (photoCursor != null && photoCursor.moveToFirst()) {
            do {
                String path = photoCursor.getString(photoCursor.getColumnIndex(PHOTO_SECOND_COL));
                photos.add(path);
            } while (photoCursor.moveToNext());
            photoCursor.close();
        }

        String selection = CATLOC_FIRST_COL + " = ? ";
        String[] selectionArgs = {String.valueOf(idPlace)};

        Cursor c = db.query(
                CATLOC_TABLE_NAME,
                null,
                selection,
                selectionArgs,
                null,
                null,
                CATLOC_FIRST_COL);

        List<Category> categories = new ArrayList<>();
        if (c != null && c.moveToFirst()) {
            do {
                int idCat = c.getInt(c.getColumnIndex(CATLOC_SECOND_COL));
                categories.add(Category.getCategory(db, idCat, null));
            } while (c.moveToNext());
            c.close();
        }

        return new Place(idPlace, name, desc, rating, location, categories, photos);
    }

    public static void updatePlace(SQLiteDatabase db, final int id,
                                   final String name, final String description, final int rating,
                                   final LatLng latLng, final List<String> categories,
                                   final List<String> images) {

        ContentValues values = new ContentValues();
        values.put(LOCAL_SECOND_COL, name);
        values.put(LOCAL_THIRD_COL, description);
        values.put(LOCAL_FORTH_COL, rating);
        values.put(LOCAL_FIFTH_COL, latLng.latitude);
        values.put(LOCAL_SIXTH_COL, latLng.longitude);

        String where = LOCAL_FIRST_COL + " = ?";
        String[] args = {String.valueOf(id)};
        db.update(LOCAL_TABLE_NAME, values, where, args);

        db.delete(PHOTO_TABLE_NAME, PHOTO_THIRD_COL + " =?", new String[]{String.valueOf(id)});
        ContentValues photoValues = new ContentValues();
        int imgCount = images.size();
        for (int i = 0; i < imgCount; i++) {
            photoValues.put(PHOTO_SECOND_COL, images.get(i));
            photoValues.put(PHOTO_THIRD_COL, id);
            db.insert(PHOTO_TABLE_NAME, null, photoValues);
        }

        db.delete(CATLOC_TABLE_NAME, CATLOC_FIRST_COL + " =?", new String[]{String.valueOf(id)});
        ContentValues catValues = new ContentValues();
        int catCount = categories.size();
        for (int i = 0; i < catCount; i++) {
            String selection = CAT_SECOND_COL + " = ?";
            String[] selectionArgs = {categories.get(i)};

            Cursor cursor = db.query(
                    CAT_TABLE_NAME,
                    null,
                    selection,
                    selectionArgs,
                    null,
                    null,
                    CAT_FIRST_COL);

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    int idCat = cursor.getInt(cursor.getColumnIndex(CAT_FIRST_COL));
                    catValues.put(CATLOC_SECOND_COL, idCat);
                    catValues.put(CATLOC_FIRST_COL, id);
                    db.insert(CATLOC_TABLE_NAME, null, catValues);
                } while (cursor.moveToNext());
                cursor.close();
            }
        }
    }

    public static void searchByCategory(SQLiteDatabase db, List<Place> list, final String by,
                                        boolean exact) {
        List<Category> categories = new ArrayList<>();
        Category.getCategories(db, categories, by, exact);

        for (int i = 0; i < categories.size(); i++) {
            String selection = CATLOC_SECOND_COL + " = ? ";
            String[] selectionArgs = new String[]{String.valueOf(categories.get(i).getId())};
            Cursor placeLocCursor = db.query(
                    CATLOC_TABLE_NAME,
                    null,
                    selection,
                    selectionArgs,
                    null,
                    null,
                    CATLOC_FIRST_COL);
            if (placeLocCursor != null && placeLocCursor.moveToFirst()) {
                do {
                    int idPlace =
                            placeLocCursor.getInt(placeLocCursor.getColumnIndex(CATLOC_FIRST_COL));
                    list.add(Place.getPlace(db, idPlace));
                } while (placeLocCursor.moveToNext());
                placeLocCursor.close();
            }
        }
    }

    private static double distanceFrom(double latitude, double longitude, double latitude2, double longitude2) {
        double R = 6371; // km
        double dLat = Math.toRadians(latitude - latitude2);
        double dLon = Math.toRadians(longitude - longitude2);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(latitude)) * Math.cos(Math.toRadians(latitude)) *
                        Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return R * c;
    }

    public static void closestPlaces(List<Place> list, double current_lat, double current_lng) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            list.get(i)._distance = distanceFrom(current_lat, current_lng,
                    list.get(i).getLocation().latitude,
                    list.get(i).getLocation().longitude);
        }

        Collections.sort(list, (o1, o2) -> Double.compare(o1._distance, o2._distance));
    }

    public static void deletePlace(SQLiteDatabase db, int id_place) {
        String selection = LOCAL_FIRST_COL + " = ?";
        String catlocSelection = CATLOC_FIRST_COL + " = ?";
        String caminhoSelection = CAMINHO_SECOND_COL + " = ?";

        String[] selectionArgs = {String.valueOf(id_place)};

        db.delete(CAMINHO_TABLE_NAME, caminhoSelection, selectionArgs);
        db.delete(CATLOC_TABLE_NAME, catlocSelection, selectionArgs);
        db.delete(LOCAL_TABLE_NAME, selection, selectionArgs);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public LatLng getLocation() {
        return latLng;
    }

    public void setLocation(LatLng location) {
        this.latLng = location;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    public double get_distance() {
        return _distance;
    }

    public void set_distance(double _distance) {
        this._distance = _distance;
    }
}
