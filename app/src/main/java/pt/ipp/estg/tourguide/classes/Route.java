package pt.ipp.estg.tourguide.classes;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import static pt.ipp.estg.tourguide.others.DataBaseHelper.CAMINHO_FIRST_COL;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.CAMINHO_SECOND_COL;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.CAMINHO_TABLE_NAME;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.ROTA_FIRST_COL;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.ROTA_SECOND_COL;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.ROTA_TABLE_NAME;
import static pt.ipp.estg.tourguide.others.DataBaseHelper.ROTA_THIRD_COL;

public class Route {
    private int id;
    private String nome;
    private String desc;
    private List<Place> places;

    public Route(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Route(int id, String nome, String desc, List<Place> places) {
        this.id = id;
        this.nome = nome;
        this.desc = desc;
        this.places = places;
    }

    public static void createRoute(SQLiteDatabase db, final String name, final String desc,
                                   final List<Integer> places) {
        ContentValues values = new ContentValues();
        values.put(ROTA_SECOND_COL, name);
        values.put(ROTA_THIRD_COL, desc);
        db.insert(ROTA_TABLE_NAME, null, values);

        String rotaOrder = ROTA_FIRST_COL + " DESC ";
        Cursor rotaCursor = db.query(
                ROTA_TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                rotaOrder,
                "1");

        int rotaId = -1;
        if (rotaCursor != null && rotaCursor.moveToFirst()) {
            rotaId = rotaCursor.getInt(rotaCursor.getColumnIndex(ROTA_FIRST_COL));
            rotaCursor.close();
        }

        int size = places.size();
        for (int i = 0; i < size; i++) {
            ContentValues caminhoValues = new ContentValues();
            caminhoValues.put(CAMINHO_FIRST_COL, rotaId);
            caminhoValues.put(CAMINHO_SECOND_COL, places.get(i));
            db.insert(CAMINHO_TABLE_NAME, null, caminhoValues);
        }

    }

    public static void getRoutes(SQLiteDatabase db, List<Route> list) {
        Cursor cursor = db.query(
                ROTA_TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(cursor.getColumnIndex(ROTA_FIRST_COL));
                String name = cursor.getString(cursor.getColumnIndex(ROTA_SECOND_COL));
                list.add(new Route(id, name));
            } while (cursor.moveToNext());
            cursor.close();
        }
    }

    public static Route getRoute(SQLiteDatabase db, final int id_route) {
        String selection = ROTA_FIRST_COL + " = ? ";
        String[] selectionArgs = {String.valueOf(id_route)};

        Cursor cursor = db.query(
                ROTA_TABLE_NAME,
                null,
                selection,
                selectionArgs,
                null,
                null,
                ROTA_FIRST_COL);

        if (cursor != null && cursor.moveToFirst()) {
            Route route;

            int routeId = cursor.getInt(cursor.getColumnIndex(ROTA_FIRST_COL));
            String name = cursor.getString(cursor.getColumnIndex(ROTA_SECOND_COL));
            String desc = cursor.getString(cursor.getColumnIndex(ROTA_THIRD_COL));

            String caminhoSelection = CAMINHO_FIRST_COL + " = ? ";
            String[] caminhoSelectionArgs = {String.valueOf(routeId)};

            Cursor caminhoCursor = db.query(
                    CAMINHO_TABLE_NAME,
                    null,
                    caminhoSelection,
                    caminhoSelectionArgs,
                    null,
                    null,
                    null);

            List<Place> places = new ArrayList<>();
            if (caminhoCursor != null && caminhoCursor.moveToFirst()) {
                do {
                    int placeId = caminhoCursor.getInt(
                            caminhoCursor.getColumnIndex(CAMINHO_SECOND_COL));
                    places.add(Place.getPlace(db, placeId));
                } while (caminhoCursor.moveToNext());

                caminhoCursor.close();
            }
            route = new Route(routeId, name, desc, places);

            cursor.close();

            return route;
        }
        return null;
    }

    public static void deleteRoute(SQLiteDatabase db, final int id_route) {
        String whereClause = ROTA_FIRST_COL + " = ? ";
        String[] whereArgs = new String[]{String.valueOf(id_route)};
        deleteCaminhos(db, id_route);
        db.delete(ROTA_TABLE_NAME, whereClause, whereArgs);
    }

    private static void deleteCaminhos(SQLiteDatabase db, final int id_rota) {
        String selection = CAMINHO_FIRST_COL + " = ?";
        String[] selectionArgs = {String.valueOf(id_rota)};
        db.delete(CAMINHO_TABLE_NAME, selection, selectionArgs);
    }

    public static void updateRoute(SQLiteDatabase db, final int idRoute, final String name, final String desc,
                                   final List<Integer> places) {

        String selection = ROTA_FIRST_COL + " = ?";
        String selectionArgs[] = {String.valueOf(idRoute)};
        ContentValues values = new ContentValues();
        values.put(ROTA_SECOND_COL, name);
        values.put(ROTA_THIRD_COL, desc);
        db.update(ROTA_TABLE_NAME, values, selection, selectionArgs);

        selection = CAMINHO_FIRST_COL + " = ?";
        db.delete(CAMINHO_TABLE_NAME, selection, selectionArgs);

        int size = places.size();
        for (int i = 0; i < size; i++) {
            ContentValues caminhoValues = new ContentValues();
            caminhoValues.put(CAMINHO_FIRST_COL, idRoute);
            caminhoValues.put(CAMINHO_SECOND_COL, places.get(i));
            db.insert(CAMINHO_TABLE_NAME, null, caminhoValues);
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<Place> getPlaces() {
        return places;
    }

    public void setPlaces(List<Place> places) {
        this.places = places;
    }
}
