package pt.ipp.estg.tourguide.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import pt.ipp.estg.tourguide.R;
import pt.ipp.estg.tourguide.classes.Category;
import pt.ipp.estg.tourguide.others.DataBaseHelper;

public class CategoryDetailsFragment extends Fragment {

    private DataBaseHelper helper;
    private Listener listener;

    public CategoryDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.listener = (Listener) context;
        this.helper = new DataBaseHelper(context);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_category_details, container, false);

        int catPos = listener.setCatPos();
        Category category = helper.getCategory(catPos, null);

        TextView name = layout.findViewById(R.id.name);
        name.setText(category.getNome());

        TextView desc = layout.findViewById(R.id.desc);
        if (category.getDesc().length() > 0) {
            desc.setText(category.getDesc());
        } else {
            desc.setText(R.string.no_desc);
        }
        return layout;
    }

    public interface Listener {
        int setCatPos();

    }
}