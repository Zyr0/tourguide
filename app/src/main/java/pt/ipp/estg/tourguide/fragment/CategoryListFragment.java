package pt.ipp.estg.tourguide.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pt.ipp.estg.tourguide.R;
import pt.ipp.estg.tourguide.activity.DetailActivity;
import pt.ipp.estg.tourguide.classes.Category;
import pt.ipp.estg.tourguide.others.DataBaseHelper;
import pt.ipp.estg.tourguide.others.ListAdapter;

public class CategoryListFragment extends Fragment implements ListAdapter.Listener {

    private List<Category> list;
    private Context context;
    private ListAdapter adapter;
    private DataBaseHelper helper;
    private View layout;

    public CategoryListFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        layout = inflater
                .inflate(R.layout.fragment_list, container, false);

        RecyclerView recyclerView = layout.findViewById(R.id.recycler_view);

        list = new ArrayList<>();

        helper = new DataBaseHelper(context);
        helper.getCategories(list);

        if (list.size() > 0) {
            adapter = new ListAdapter(list, ListAdapter.CATEGORIES);
            adapter.setListener(this);
            recyclerView.setAdapter(adapter);

            LinearLayoutManager manager = new LinearLayoutManager(context);
            recyclerView.setLayoutManager(manager);
        } else {
            TextView nothing = layout.findViewById(R.id.nothing_show);
            nothing.setVisibility(View.VISIBLE);
        }

        return layout;
    }

    @Override
    public void onClick(int pos) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra("cat", true);
        intent.putExtra("catPos", list.get(pos).getId());
        startActivity(intent);
    }

    @Override
    public void longClick(int pos) {
        String title = getString(R.string.delete_init) + " " + list.get(pos).getNome() + "?";
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(R.string.message_delete)
                .setPositiveButton(R.string.btn_ok, (dialog, id) -> {
                    helper.deleteCategory(list.get(pos).getId());
                    list.remove(pos);
                    if (list.size() == 0) {
                        TextView nothing = layout.findViewById(R.id.nothing_show);
                        nothing.setVisibility(View.VISIBLE);
                    }
                    adapter.notifyItemRemoved(pos);
                    adapter.notifyItemRangeChanged(pos, list.size());
                    Toast.makeText(context, R.string.deleted_success, Toast.LENGTH_SHORT).show();
                })
                .setNegativeButton(R.string.btn_cancel, (dialog, id) -> dialog.dismiss());
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
