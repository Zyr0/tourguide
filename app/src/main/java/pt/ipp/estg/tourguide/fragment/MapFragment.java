package pt.ipp.estg.tourguide.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pt.ipp.estg.tourguide.R;
import pt.ipp.estg.tourguide.activity.DetailActivity;
import pt.ipp.estg.tourguide.classes.Place;
import pt.ipp.estg.tourguide.others.DataBaseHelper;
import pt.ipp.estg.tourguide.tasks.UrlFetcher;

/**
 * A simple {@link SupportMapFragment} subclass.
 */
public class MapFragment extends SupportMapFragment
        implements OnMapReadyCallback, LocationListener, UrlFetcher.Listener {

    private static final int PERMISSION_REQUEST_CODE = 100;
    private Activity activity;
    private Context context;
    private LocationManager manager;
    private GoogleMap map;
    private MarkerOptions markerOptions;
    private PolylineOptions polylineOptions;
    private HashMap<Marker, Integer> hashMap;
    private List<MarkerOptions> markers;
    private List<Place> places;
    private int route = -1;

    public MapFragment() {
        // Required empty public constructor
    }

    public void setMarker(final Place place) {
        markerOptions = new MarkerOptions();
        String title = place.getNome();
        markerOptions.snippet(place.getDesc());
        markerOptions.title(title);
        markerOptions.position(place.getLocation());
    }

    public void setMarkers(final List<Place> list) {
        this.places = list;
    }

    public void setRoute(final List<Place> list, final int routeId) {
        route = routeId;
        UrlFetcher fetcher = new UrlFetcher();
        fetcher.setListener(this);
        int size = list.size();
        LatLng[] coordinates = new LatLng[size];
        for (int i = 0; i < size; i++) {
            Place p = list.get(i);
            coordinates[i] = p.getLocation();
        }
        fetcher.execute(coordinates);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.activity = getActivity();
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        super.getMapAsync(this);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        boolean isSettable = false;

        if (markerOptions != null && markerOptions.getSnippet().equals("")) {
            String snippet = context.getString(R.string.no_desc);
            markerOptions.snippet(snippet);
        }

        if (bundle != null) {
            double lat = bundle.getDouble("location_lat", 999);
            double lng = bundle.getDouble("location_lng", 999);
            String title = bundle.getString("title", "");
            String snippet = bundle.getString("snippet", "");

            if (lat != 999 && lng != 999) {
                markerOptions = new MarkerOptions();
                markerOptions.title(title);
                markerOptions.snippet(snippet);
                markerOptions.position(new LatLng(lat, lng));
            }

            int route = bundle.getInt("route", -1);
            if (route != -1) {
                this.route = route;
                isSettable = true;
            }
        }

        if (route != -1) {
            DataBaseHelper helper = new DataBaseHelper(context);
            List<Place> list = helper.getRoute(route).getPlaces();
            if (isSettable) {
                setRoute(list, route);
            }
            places = list;
            addMarkers(list);
        }

        if (places != null && this.places.size() > 0) {
            addMarkers(places);
        }
        manager = (LocationManager) this.context
                .getSystemService(Context.LOCATION_SERVICE);
    }

    private void addMarkers(List<Place> list) {
        markers = new ArrayList<>();
        String snippet = context.getString(R.string.no_desc);
        int size = list.size();
        for (int i = 0; i < size; i++) {
            Place p = list.get(i);
            if (p.getDesc() != null && !p.getDesc().equals("")) {
                snippet = p.getDesc();
            }
            markers.add(new MarkerOptions()
                    .title(p.getNome())
                    .snippet(snippet)
                    .position(p.getLocation()));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ActivityCompat
                .checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this.activity,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_CODE);
            return;
        }
        manager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, 0, 0, this);
    }

    @Override
    public void onStop() {
        super.onStop();
        manager.removeUpdates(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.getUiSettings().setZoomControlsEnabled(true);
        int fineLocation = ActivityCompat
                .checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        if (fineLocation != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        map.setMyLocationEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);
        if (markerOptions != null) {
            map.addMarker(markerOptions);
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(markerOptions.getPosition(), 10));
        } else if (polylineOptions != null) {
            addPolyline();
        } else if (places.size() > 0) {
            addMarkersToMap();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (bundle != null) {
            if (markerOptions != null) {
                bundle.putDouble("location_lat", markerOptions.getPosition().latitude);
                bundle.putDouble("location_lng", markerOptions.getPosition().longitude);
                bundle.putString("title", markerOptions.getTitle());
                bundle.putString("snippet", markerOptions.getSnippet());
            } else {
                bundle.putInt("route", route);
            }
        }
    }

    @Override
    public void onPostExecute(List<LatLng> s) {
        if (s != null) {
            polylineOptions = new PolylineOptions();
            polylineOptions.addAll(s);
            polylineOptions.width(15);
            polylineOptions.color(Color.BLUE);
            if (map != null) {
                addPolyline();
            }
        } else {
            Toast.makeText(activity, "Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }

    private void addPolyline() {
        map.addPolyline(polylineOptions);
        addMarkersToMap();
    }

    private void addMarkersToMap() {
        hashMap = new HashMap<>();
        int size = markers.size();
        for (int i = 0; i < size; i++) {
            Marker marker = map.addMarker(markers.get(i));
            hashMap.put(marker, places.get(i).getId());
        }
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(markers.get(0).getPosition(), 14));
        map.setOnInfoWindowClickListener(marker -> {
            int pos = hashMap.get(marker);
            Intent i = new Intent(context, DetailActivity.class);
            i.putExtra("placePos", pos);
            startActivity(i);
        });
    }
}
