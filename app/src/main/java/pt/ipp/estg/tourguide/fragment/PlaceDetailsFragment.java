package pt.ipp.estg.tourguide.fragment;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;

import eu.fiskur.simpleviewpager.SimpleViewPager;
import pt.ipp.estg.tourguide.R;
import pt.ipp.estg.tourguide.classes.Place;
import pt.ipp.estg.tourguide.others.DataBaseHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlaceDetailsFragment extends Fragment {

    private Listener listener;
    private DataBaseHelper helper;
    private SimpleViewPager simpleViewPager;

    public PlaceDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.listener = (Listener) context;
        helper = new DataBaseHelper(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_place_details, container, false);

        int pos = listener.setPlacePos();
        Place place = helper.getPlace(pos);

        TextView name = layout.findViewById(R.id.name);
        name.setText(place.getNome());

        TextView rating = layout.findViewById(R.id.rating);
        rating.append(" " + place.getRating() + " ");

        simpleViewPager = layout.findViewById(R.id.simple_view_pager);

        String[] imgUrl = place.getPhotos().toArray(new String[0]);
        simpleViewPager.setImageUrls(imgUrl,
                (imageView, s) -> Picasso.get().load(new File(s)).into(imageView));

        int indicatorColor = Color.parseColor("#ffffff");
        int selectedIndicatorColor = Color.parseColor("#AB2C3A");
        simpleViewPager.showIndicator(indicatorColor, selectedIndicatorColor);

        TextView desc = layout.findViewById(R.id.desc);
        if (place.getDesc().equals("")) {
            desc.setText(R.string.no_desc);
        } else {
            desc.setText(place.getDesc());
        }

        TextView categories = layout.findViewById(R.id.categories);

        for (int i = 0; i < place.getCategories().size(); i++) {
            categories.append(place.getCategories().get(i).getNome());
            if (place.getCategories().size() - 1 > i) {
                categories.append(" | ");
            }
        }

        Button btnLocation = layout.findViewById(R.id.btn_location);
        btnLocation.setOnClickListener(v -> {
            if (listener != null) {
                listener.onPlaceClick(v, place);
            }
        });
        return layout;
    }

    @Override
    public void onStop() {
        simpleViewPager.clearListeners();
        super.onStop();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.listener = null;
    }

    public interface Listener {
        int setPlacePos();

        void onPlaceClick(View v, Place place);
    }
}
