package pt.ipp.estg.tourguide.fragment;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import pt.ipp.estg.tourguide.R;
import pt.ipp.estg.tourguide.activity.DetailActivity;
import pt.ipp.estg.tourguide.classes.Place;
import pt.ipp.estg.tourguide.others.DataBaseHelper;
import pt.ipp.estg.tourguide.others.ListAdapter;
import pt.ipp.estg.tourguide.services.LocationService;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlacesListFragment extends Fragment implements
        ListAdapter.Listener, View.OnClickListener {

    private List<Place> list;
    private Context context;
    private ListAdapter adapter;
    private DataBaseHelper helper;
    private LinearLayout btns_layout;
    private Toast toast;
    private View layout;

    private ImageButton[] btn = new ImageButton[5];
    private ImageButton btn_unfocus;
    private int[] btn_id = {R.id.sort_recent, R.id.sort_az, R.id.sort_za,
            R.id.sort_rate, R.id.sort_myLocation};

    private LocationService locationService;
    private boolean isServiceBound;

    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isServiceBound = false;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocationService.MyBinder myBinder = (LocationService.MyBinder) service;
            locationService = myBinder.getService();
            isServiceBound = true;
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(context, LocationService.class);
        context.bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (isServiceBound) {
            context.unbindService(mServiceConnection);
            isServiceBound = false;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        layout = inflater
                .inflate(R.layout.fragment_places_list, container, false);

        RecyclerView recyclerView = layout.findViewById(R.id.recycler_view);

        btns_layout = layout.findViewById(R.id.sort_btns);
        for (int i = 0; i < btn.length; i++) {
            btn[i] = layout.findViewById(btn_id[i]);
            btn[i].setOnClickListener(this);
        }
        btn_unfocus = btn[0];
        setFocus(btn_unfocus, btn[0]);

        list = new ArrayList<>();

        helper = new DataBaseHelper(context);

        helper.getPlaces(list, "recent", false);

        if (list.size() > 0) {
            adapter = new ListAdapter(list, ListAdapter.PLACES);
            adapter.setListener(this);
            recyclerView.setAdapter(adapter);

            if (list.size() > 1) btns_layout.setVisibility(View.VISIBLE);

            LinearLayoutManager manager = new LinearLayoutManager(context);
            recyclerView.setLayoutManager(manager);
        } else {
            TextView nothing = layout.findViewById(R.id.nothing_show);
            nothing.setVisibility(View.VISIBLE);
        }
        return layout;
    }

    @Override
    public void onClick(int pos) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra("placePos", list.get(pos).getId());
        startActivity(intent);
    }

    @Override
    public void longClick(int pos) {
        String title = getString(R.string.delete_init) + " " + list.get(pos).getNome() + "?";
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(R.string.message_delete)
                .setPositiveButton(R.string.btn_ok, (dialog, id) -> {
                    helper.deletePlace(list.get(pos).getId());
                    list.remove(pos);
                    if (list.size() == 0) {
                        TextView nothing = layout.findViewById(R.id.nothing_show);
                        nothing.setVisibility(View.VISIBLE);
                    } else if (list.size() < 2) btns_layout.setVisibility(View.GONE);
                    adapter.notifyItemRemoved(pos);
                    adapter.notifyItemRangeChanged(pos, list.size());
                    showToast(getString(R.string.deleted_success));
                })
                .setNegativeButton(R.string.btn_cancel, (dialog, id) -> dialog.dismiss());

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sort_recent:
                setFocus(btn_unfocus, btn[0]);
                helper.getPlaces(list, "recent", false);
                adapter.notifyDataSetChanged();
                showToast(getString(R.string.sort_recent));
                break;
            case R.id.sort_az:
                setFocus(btn_unfocus, btn[1]);
                helper.getPlaces(list, "a-z", false);
                adapter.notifyDataSetChanged();
                showToast(getString(R.string.sort_az));
                break;
            case R.id.sort_za:
                setFocus(btn_unfocus, btn[2]);
                helper.getPlaces(list, "z-a", false);
                adapter.notifyDataSetChanged();
                showToast(getString(R.string.sort_za));
                break;
            case R.id.sort_rate:
                setFocus(btn_unfocus, btn[3]);
                helper.getPlaces(list, "rating", false);
                adapter.notifyDataSetChanged();
                showToast(getString(R.string.sort_rate));
                break;
            case R.id.sort_myLocation:
                setFocus(btn_unfocus, btn[4]);
                LatLng location = locationService.getCoordinates();
                Place.closestPlaces(list, location.latitude, location.longitude);
                adapter.notifyDataSetChanged();
                showToast(getString(R.string.sort_by_my_location));
                break;
        }
    }

    @SuppressLint("ShowToast")
    private void showToast(String text) {
        if (toast == null) {
            toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        } else {
            toast.setText(text);
        }
        toast.show();
    }

    private void setFocus(ImageButton btn_unfocus, ImageButton btn_focus) {
        btn_unfocus.setBackgroundColor(Color.rgb(50, 50, 50));
        btn_focus.setBackgroundColor(Color.rgb(171, 44, 58));
        this.btn_unfocus = btn_focus;
    }
}
