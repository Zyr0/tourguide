package pt.ipp.estg.tourguide.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import pt.ipp.estg.tourguide.R;
import pt.ipp.estg.tourguide.activity.DetailActivity;
import pt.ipp.estg.tourguide.classes.Place;
import pt.ipp.estg.tourguide.classes.Route;
import pt.ipp.estg.tourguide.others.DataBaseHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class RouteDetailsFragment extends Fragment {

    private DataBaseHelper helper;
    private Context context;
    private Listener listener;

    public RouteDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        helper = new DataBaseHelper(context);
        this.listener = (Listener) context;
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_route_details, container, false);

        int pos = listener.setRoutePos();
        Route route = helper.getRoute(pos);

        TextView name = layout.findViewById(R.id.name);
        name.setText(route.getNome());

        TextView desc = layout.findViewById(R.id.desc);
        if (route.getDesc().length() > 0) {
            desc.setText(route.getDesc());
        } else {
            desc.setText(R.string.no_desc);
        }

        layout.findViewById(R.id.btn_show_route).setOnClickListener(v -> {
            if (listener != null) {
                listener.onRouteClick(v, route.getPlaces());
            }
        });

        layout.findViewById(R.id.btn_show_places).setOnClickListener(v -> {
            int size = route.getPlaces().size();
            String[] places = new String[size];
            for (int i = 0; i < size; i++) {
                places[i] = route.getPlaces().get(i).getNome();
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(R.string.places_tab);
            builder.setItems(places, (dialog, which) -> {
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("placePos", route.getPlaces().get(which).getId());
                startActivity(intent);
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        });

        return layout;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.listener = null;
    }

    public interface Listener {
        int setRoutePos();

        void onRouteClick(View v, List<Place> list);
    }
}
