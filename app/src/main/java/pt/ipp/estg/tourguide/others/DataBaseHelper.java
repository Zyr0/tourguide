package pt.ipp.estg.tourguide.others;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import pt.ipp.estg.tourguide.classes.Category;
import pt.ipp.estg.tourguide.classes.Place;
import pt.ipp.estg.tourguide.classes.Route;

public class DataBaseHelper extends SQLiteOpenHelper {
    public static final String PHOTO_TABLE_NAME = "photo";
    public static final String PHOTO_FIRST_COL = "id";
    public static final String PHOTO_SECOND_COL = "path";
    public static final String PHOTO_THIRD_COL = "local_id";
    public static final String CAT_FIRST_COL = "id";
    public static final String CAT_TABLE_NAME = "cat";
    public static final String CAT_SECOND_COL = "nome";
    public static final String CAT_THIRD_COL = "descricao";
    public static final String LOCAL_TABLE_NAME = "local";
    public static final String LOCAL_FIRST_COL = "id";
    public static final String LOCAL_SECOND_COL = "nome";
    public static final String LOCAL_THIRD_COL = "descricao";
    public static final String LOCAL_FORTH_COL = "rating";
    public static final String LOCAL_FIFTH_COL = "lat";
    public static final String LOCAL_SIXTH_COL = "lng";
    public static final String ROTA_TABLE_NAME = "rota";
    public static final String ROTA_FIRST_COL = "id";
    public static final String ROTA_SECOND_COL = "nome";
    public static final String ROTA_THIRD_COL = "descricao";
    public static final String CAMINHO_TABLE_NAME = "caminho";
    public static final String CAMINHO_FIRST_COL = "id_rota";
    public static final String CAMINHO_SECOND_COL = "id_local";
    public static final String CATLOC_TABLE_NAME = "catloc";
    public static final String CATLOC_FIRST_COL = "id_loc";
    public static final String CATLOC_SECOND_COL = "id_cat";
    private static final String DATABASE_NAME = "tour";
    private static final int DATABASE_VERSION = 8;

    public DataBaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + PHOTO_TABLE_NAME + "( " +
                PHOTO_FIRST_COL + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                PHOTO_SECOND_COL + " VARCHAR(100) NOT NULL, " +
                PHOTO_THIRD_COL + " VARCHAR(250) NOT NULL)");

        db.execSQL("CREATE TABLE IF NOT EXISTS " + CAT_TABLE_NAME + "( " +
                CAT_FIRST_COL + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                CAT_SECOND_COL + " VARCHAR(100) DEFAULT NULL, " +
                CAT_THIRD_COL + " TEXT)");

        db.execSQL("CREATE TABLE IF NOT EXISTS " + LOCAL_TABLE_NAME + "( " +
                LOCAL_FIRST_COL + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                LOCAL_SECOND_COL + " VARCHAR(100) NOT NULL, " +
                LOCAL_THIRD_COL + " TEXT NOT NULL, " +
                LOCAL_FORTH_COL + " INTEGER NOT NULL, " +
                LOCAL_FIFTH_COL + " DOUBLE NOT NULL, " +
                LOCAL_SIXTH_COL + " DOUBLE NOT NULL)");

        db.execSQL("CREATE TABLE IF NOT EXISTS " + ROTA_TABLE_NAME + "( " +
                ROTA_FIRST_COL + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                ROTA_SECOND_COL + " VARCHAR(100) NOT NULL, " +
                ROTA_THIRD_COL + " TEXT)");

        db.execSQL("CREATE TABLE IF NOT EXISTS " + CAMINHO_TABLE_NAME + "( " +
                CAMINHO_FIRST_COL + " INTEGER NOT NULL ," + // "REFERENCES " + ROTA_TABLE_NAME + "(" + ROTA_FIRST_COL + ")," + //
                CAMINHO_SECOND_COL + " INTEGER NOT NULL )"); // + "REFERENCES " + LOCAL_TABLE_NAME + "(" + LOCAL_FIRST_COL + "))");

        db.execSQL("CREATE TABLE IF NOT EXISTS " + CATLOC_TABLE_NAME + "( " +
                CATLOC_FIRST_COL + " INTEGER NOT NULL ," +
                CATLOC_SECOND_COL + " INTEGER NOT NULL )");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropAll(db);
        onCreate(db);
    }

    private void dropAll(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + PHOTO_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + CAT_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + LOCAL_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ROTA_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + CAMINHO_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + CATLOC_TABLE_NAME);
    }

    //places
    public void createPlace(String name, String description, int rating, LatLng latLng,
                            List<String> categories, List<String> images) {
        SQLiteDatabase db = getWritableDatabase();
        Place.createPlace(db, name, description, rating, latLng, categories, images);
        db.close();
    }

    public void getPlaces(List<Place> list, String by, boolean search) {
        SQLiteDatabase db = getReadableDatabase();
        Place.getPlaces(db, list, by, search);
        db.close();
    }

    public Place getPlace(int id_place) {
        SQLiteDatabase db = getReadableDatabase();
        Place place = Place.getPlace(db, id_place);
        db.close();
        return place;
    }

    public void updatePlace(int id, String name, String description, int rating, LatLng latLng,
                            List<String> categories, List<String> images) {
        SQLiteDatabase db = getWritableDatabase();
        Place.updatePlace(db, id, name, description, rating, latLng, categories, images);
        db.close();
    }

    public void deletePlace(int id_place) {
        SQLiteDatabase db = getReadableDatabase();
        Place.deletePlace(db, id_place);
        db.close();
    }

    public void getCategoriesByPlace(List<Category> list, int id_place) {
        SQLiteDatabase db = getWritableDatabase();
        Category.getCategoryByPlace(db, list, id_place);
        db.close();
    }

    public void searchByCategory(List<Place> list, String by, boolean exact) {
        SQLiteDatabase db = getReadableDatabase();
        Place.searchByCategory(db, list, by, exact);
        db.close();
    }

    // categories
    public void createCategory(String name, String description) {
        SQLiteDatabase db = getWritableDatabase();
        Category.createCategory(db, name, description);
        db.close();
    }

    public void updateCategory(int id, String name, String description) {
        SQLiteDatabase db = getWritableDatabase();
        Category.updateCategory(db, id, name, description);
        db.close();
    }

    public void getCategories(List<Category> list) {
        SQLiteDatabase db = getWritableDatabase();
        Category.getCategories(db, list, null, false);
        db.close();
    }

    public Category getCategory(int pos, String nome) {
        SQLiteDatabase db = getReadableDatabase();
        Category category = Category.getCategory(db, pos, nome);
        db.close();
        return category;
    }

    public void deleteCategory(int id) {
        SQLiteDatabase db = getReadableDatabase();
        Category.deleteCategory(db, id);
        db.close();
    }

    // routes
    public void createRoute(String name, String desc, List<Integer> places) {
        SQLiteDatabase db = getWritableDatabase();
        Route.createRoute(db, name, desc, places);
        db.close();
    }

    public void updateRoute(int idRoute, String name, String desc, List<Integer> places) {
        SQLiteDatabase db = getWritableDatabase();
        Route.updateRoute(db, idRoute, name, desc, places);
        db.close();
    }

    public void getRoutes(List<Route> list) {
        SQLiteDatabase db = getReadableDatabase();
        Route.getRoutes(db, list);
        db.close();
    }

    public Route getRoute(int id_route) {
        SQLiteDatabase db = getReadableDatabase();
        Route route = Route.getRoute(db, id_route);
        db.close();
        return route;
    }

    public void deleteRoute(int id_route) {
        SQLiteDatabase db = getWritableDatabase();
        Route.deleteRoute(db, id_route);
        db.close();
    }
}