package pt.ipp.estg.tourguide.others;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import pt.ipp.estg.tourguide.R;
import pt.ipp.estg.tourguide.classes.Category;
import pt.ipp.estg.tourguide.classes.Place;
import pt.ipp.estg.tourguide.classes.Route;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    public static final String PLACES = "Places";
    public static final String ROUTES = "Routes";
    public static final String CATEGORIES = "Categories";
    private final List<?> list;
    private Listener listener;
    private String type;

    public ListAdapter(List<?> list, String type) {
        this.list = list;
        this.type = type;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layout = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row, viewGroup, false);
        return new ViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        View layout = viewHolder.layout;
        layout.setLongClickable(true);

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        TextView name = layout.findViewById(R.id.name);
        ImageView img = layout.findViewById(R.id.img);

        switch (type) {
            case PLACES:
                Place place = (Place) list.get(position);
                name.setText(place.getNome());
                String path = place.getPhotos().get(0);
                Picasso.get().load(new File(path)).into(img);
                break;
            case CATEGORIES:
                Category cat = (Category) list.get(position);
                img.setImageDrawable(
                        ContextCompat.getDrawable(layout.getContext(), R.drawable.ic_category));
                name.setText(cat.getNome());
                break;
            case ROUTES:
                Route route = (Route) list.get(position);
                img.setImageDrawable(
                        ContextCompat.getDrawable(layout.getContext(), R.drawable.ic_map));
                name.setText(route.getNome());
                break;
            default:
                name.setText(this.list.get(position).toString());
        }

        layout.setOnClickListener(v -> {
            if (listener != null) {
                listener.onClick(position);
            }
        });

        layout.setOnLongClickListener(v -> {
            if (listener != null) {
                listener.longClick(position);
                return true;
            }
            return false;
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface Listener {
        void onClick(int pos);

        void longClick(int pos);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private View layout;

        ViewHolder(@NonNull View v) {
            super(v);
            layout = v;
        }
    }
}
