package pt.ipp.estg.tourguide.others;

import android.app.Activity;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;

import pt.ipp.estg.tourguide.R;

public class TextWatcherHelper implements TextWatcher {

    public static final String NAME = "__name__";
    public static final String LAT = "__lat__";
    public static final String LNG = "__lng__";

    private Activity context;
    private TextInputEditText textEdit;
    private TextInputLayout textLayout;
    private String type;

    public TextWatcherHelper(Activity context, TextInputEditText textEdit,
                             TextInputLayout textLayout, String type) {
        this.context = context;
        this.textEdit = textEdit;
        this.textLayout = textLayout;
        this.type = type;
    }

    private void validateName() {

        if (textEdit.getText() != null) {
            String name, text;
            text = textEdit.getText().toString();
            name = text.trim();
            if (name.length() < 3) {
                textLayout.setError(context.getString(R.string.err_msg_name));
                requestFocus(textEdit);
            } else if (name.matches(".*\\d+.*")) {
                textLayout.setError(context.getString(R.string.err_name_number));
                requestFocus(textEdit);
            } else {
                textLayout.setErrorEnabled(false);
            }
        }
    }

    private void validateLat() {
        double lat = 0;

        if (textEdit.getText() != null) {
            try {
                lat = Double.parseDouble(textEdit.getText().toString());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        if (lat < -85 || lat > 85 || textEdit.length() == 0) {
            textLayout.setError(context.getString(R.string.err_lat_invalid));
            textLayout.setErrorEnabled(true);
            requestFocus(textEdit);
        } else {
            textLayout.setErrorEnabled(false);
        }
    }

    private void validateLng() {
        double lng = 0;

        if (textEdit.getText() != null) {
            try {
                lng = Double.parseDouble(textEdit.getText().toString());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        if (lng < -180 || lng > 180 || textEdit.length() == 0) {
            textLayout.setError(context.getString(R.string.err_lng_invalid));
            textLayout.setErrorEnabled(true);
            requestFocus(textEdit);
        } else {
            textLayout.setErrorEnabled(false);
        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            context.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        switch (type) {
            case NAME:
                validateName();
                break;
            case LAT:
                validateLat();
                break;
            case LNG:
                validateLng();
                break;
        }
    }
}
