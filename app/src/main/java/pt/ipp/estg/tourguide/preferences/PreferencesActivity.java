package pt.ipp.estg.tourguide.preferences;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import pt.ipp.estg.tourguide.R;
import pt.ipp.estg.tourguide.classes.Category;
import pt.ipp.estg.tourguide.others.DataBaseHelper;

public class PreferencesActivity extends AppCompatPreferenceActivity {

    private static List<Category> list;

    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener
            preferenceChangeListener = (preference, value) -> {
        String stringValue = value.toString();

        if (preference instanceof ListPreference) {
            // For list preferences, look up the correct display value in
            // the preference's 'entries' list.
            ListPreference listPreference = (ListPreference) preference;
            int index = listPreference.findIndexOfValue(stringValue);

            // Set the summary to reflect the new value.
            preference.setSummary(
                    index >= 0
                            ? listPreference.getEntries()[index]
                            : null);

        } else {
            // For all other preferences, set the summary to the value's
            // simple string representation.
            preference.setSummary(stringValue);
        }
        return true;
    };

    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #preferenceChangeListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(preferenceChangeListener);

        // Trigger the listener immediately with the preference's
        // current value.
        preferenceChangeListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        DataBaseHelper helper = new DataBaseHelper(this);
        list = new ArrayList<>();
        helper.getCategories(list);
    }

    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this);
    }

    @Override
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.pref_headers, target);
    }

    @Override
    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || WidgetPreferenceFragment.class.getName().equals(fragmentName)
                || LocationPreferenceFragment.class.getName().equals(fragmentName);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        sendBroadcast(new Intent(getString(R.string.action_widget_update)));
    }

    public static class WidgetPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_widget);
            setHasOptionsMenu(true);

            ListPreference categoryList = (ListPreference) findPreference("category_list");

            CharSequence[] categoryEntriesList = setCategoryEntriesList(list);
            categoryList.setEntries(categoryEntriesList);
            categoryList.setEntryValues(categoryEntriesList);

            ListPreference sortList = (ListPreference) findPreference("sort_list");
            CharSequence[] sortEntriesList = new CharSequence[]{
                    "Rating 1-5", "Rating 5-1", "Proximity", "Name A-Z", "Name Z-A"};
            sortList.setEntries(sortEntriesList);
            sortList.setEntryValues(sortEntriesList);

            bindPreferenceSummaryToValue(findPreference("update_interval"));
            bindPreferenceSummaryToValue(findPreference("list_size"));
            bindPreferenceSummaryToValue(sortList);
            bindPreferenceSummaryToValue(categoryList);
        }

        private CharSequence[] setCategoryEntriesList(List<Category> list) {
            int size = list.size();
            CharSequence[] charSequences = new CharSequence[size + 1];
            for (int i = 0; i < size; i++) {
                charSequences[i] = list.get(i).getNome();
            }
            charSequences[size] = "All";
            return charSequences;
        }
    }

    public static class LocationPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_location);
            setHasOptionsMenu(true);

            bindPreferenceSummaryToValue(findPreference("search_radius"));
        }
    }
}
