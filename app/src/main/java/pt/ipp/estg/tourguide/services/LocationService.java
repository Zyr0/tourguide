package pt.ipp.estg.tourguide.services;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.maps.model.LatLng;

public class LocationService extends Service implements LocationListener {
    private IBinder binder = new MyBinder();
    private double myLat = 9999, myLng = 9999;

    public LocationService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        int fineLocation = ActivityCompat
                .checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (fineLocation != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        Location lastKnownLocation;
        LocationManager locationManager =
                (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        boolean type = PreferenceManager.getDefaultSharedPreferences(this)
                .getBoolean("location_type", false);

        if (type) {
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 0, 0, this);
        } else {
            locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, 0, 0, this);
        }

        if (myLat == 9999 || myLng == 9999) {
            if (type) {
                lastKnownLocation = locationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
            } else {
                lastKnownLocation = locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }

            if (lastKnownLocation != null) {
                myLat = lastKnownLocation.getLatitude();
                myLng = lastKnownLocation.getLongitude();
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return true;
    }

    @Override
    public void onLocationChanged(Location location) {
        myLat = location.getLatitude();
        myLng = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public LatLng getCoordinates() {
        return new LatLng(myLat, myLng);
    }

    public class MyBinder extends Binder {
        public LocationService getService() {
            return LocationService.this;
        }
    }
}
