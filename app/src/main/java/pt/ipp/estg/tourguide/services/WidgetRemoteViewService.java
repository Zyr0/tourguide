package pt.ipp.estg.tourguide.services;

import android.content.Intent;
import android.widget.RemoteViewsService;

import pt.ipp.estg.tourguide.widget.WidgetRemoteViewsFactory;

public class WidgetRemoteViewService extends RemoteViewsService {

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new WidgetRemoteViewsFactory(this, intent);
    }
}
