package pt.ipp.estg.tourguide.services;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;

import java.util.concurrent.TimeUnit;

import pt.ipp.estg.tourguide.R;

public class WidgetUpdateService extends Service {

    private Handler handler;
    private Runnable runnable;
    private int millis;

    public WidgetUpdateService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handler = new Handler();
        String stringTime = PreferenceManager.getDefaultSharedPreferences(this)
                .getString("update_interval", "30");
        this.millis = getTime(stringTime);
        runnable = () -> {
            Intent i = new Intent(getString(R.string.action_widget_update));
            sendBroadcast(i);
            String newStringTime = PreferenceManager.getDefaultSharedPreferences(this)
                    .getString("update_interval", "30");
            if (newStringTime != null && !newStringTime.equals(stringTime)) {
                this.millis = getTime(newStringTime);
            }
            handler.postDelayed(runnable, this.millis);
        };
        handler.post(runnable);
        return START_STICKY;
    }

    private int getTime(String stringTime) {
        int time = 30;
        if (stringTime != null) {
            time = Integer.valueOf(stringTime);
        }
        return (int) TimeUnit.MINUTES.toMillis(time);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
