package pt.ipp.estg.tourguide.tasks;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DataParser {

    private static final String TAG = "LOG";

    public static List<LatLng> parse(String s) throws JSONException {
        List<LatLng> list = new ArrayList<>();
        JSONObject jsonObject = new JSONObject(s);
        JSONArray coordinates = jsonObject
                .getJSONArray("routes")
                .getJSONObject(0)
                .getJSONObject("geometry")
                .getJSONArray("coordinates");
        int size = coordinates.length();
        for (int i = 0; i < size; i++) {
            JSONArray jsonLngLat = (JSONArray) coordinates.get(i);
            LatLng latLng =
                    new LatLng(jsonLngLat.getDouble(1), jsonLngLat.getDouble(0));
            list.add(latLng);
        }
        return list;
    }
}
