package pt.ipp.estg.tourguide.tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class UrlFetcher extends AsyncTask<LatLng, Void, List<LatLng>> {

    private static final String TAG = "LOG";
    private Listener listener;

    @Override
    protected List<LatLng> doInBackground(LatLng... coordinates) {
        InputStream in;
        HttpURLConnection connection = null;
        try {

            String generatedUrl = getUrl(coordinates);
            Log.d(TAG, "doInBackground: " + generatedUrl);
            // Connect
            URL url = new URL(generatedUrl.trim());
            connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(3000);
            connection.setReadTimeout(3000);
            connection.setRequestMethod("GET");

            int responseCode = connection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                in = connection.getInputStream();

                // Read Response
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
                StringBuilder builder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    builder.append(line).append("\n");
                }
                bufferedReader.close();
                in.close();

                // Parse Response
                return DataParser.parse(builder.toString());
            }
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return null;
    }

    private String getUrl(LatLng[] list) {
        String baseUrl = "https://api.openrouteservice.org/directions" +
                "?api_key=5b3ce3597851110001cf6248dc44f1d5cd0b4920ae49a902f01b9916&&coordinates=";
        String endUrl = "&profile=driving-car&geometry_format=geojson";

        int size = list.length;
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < size; i++) {
            LatLng latLng = list[i];
            builder.append(latLng.longitude).append(",").append(latLng.latitude);
            if (i < size - 1) {
                builder.append("|");
            }
        }
        return baseUrl + builder.toString() + endUrl;
    }

    @Override
    protected void onPostExecute(List<LatLng> s) {
        super.onPostExecute(s);
        listener.onPostExecute(s);
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public interface Listener {
        void onPostExecute(List<LatLng> s);
    }
}
