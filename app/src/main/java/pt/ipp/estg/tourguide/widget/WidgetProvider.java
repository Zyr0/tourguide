package pt.ipp.estg.tourguide.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.RemoteViews;

import pt.ipp.estg.tourguide.R;
import pt.ipp.estg.tourguide.activity.DetailActivity;
import pt.ipp.estg.tourguide.preferences.PreferencesActivity;
import pt.ipp.estg.tourguide.services.WidgetRemoteViewService;
import pt.ipp.estg.tourguide.services.WidgetUpdateService;

/**
 * Implementation of App WidgetProvider functionality.
 */
public class WidgetProvider extends AppWidgetProvider {

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget);

        Intent intent = new Intent(context, WidgetRemoteViewService.class);

        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));

        views.setRemoteAdapter(R.id.widgetListView, intent);

        //Intent update = new Intent(context, WidgetUpdateService.class);
        Intent update = new Intent(context.getString(R.string.action_widget_update));

        PendingIntent pendingService =
                PendingIntent.getBroadcast(context, 0, update, 0);
        views.setOnClickPendingIntent(R.id.btn_reload, pendingService);

        Intent edit = new Intent(context, PreferencesActivity.class);

        PendingIntent pendingEdit =
                PendingIntent.getActivity(context, 0, edit, 0);
        views.setOnClickPendingIntent(R.id.btn_edit, pendingEdit);

        Intent clickIntentTemplate = new Intent(context, DetailActivity.class);

        PendingIntent startActivityPendingIntent = PendingIntent.getActivity(
                context, 0, clickIntentTemplate, PendingIntent.FLAG_UPDATE_CURRENT);
        views.setPendingIntentTemplate(R.id.widgetListView, startActivityPendingIntent);

        views.setEmptyView(R.id.widgetListView, R.id.empty);
        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        context.startService(new Intent(context, WidgetUpdateService.class));
    }

    @Override
    public void onDisabled(Context context) {
        context.stopService(new Intent(context, WidgetUpdateService.class));
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        if (action != null && action.equals(context.getString(R.string.action_widget_update))) {
            AppWidgetManager manager = AppWidgetManager.getInstance(context);
            ComponentName componentName = new ComponentName(context, WidgetProvider.class);
            manager.notifyAppWidgetViewDataChanged(
                    manager.getAppWidgetIds(componentName), R.id.widgetListView);
        }
        super.onReceive(context, intent);
    }
}

