package pt.ipp.estg.tourguide.widget;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import pt.ipp.estg.tourguide.R;
import pt.ipp.estg.tourguide.classes.Place;
import pt.ipp.estg.tourguide.others.DataBaseHelper;
import pt.ipp.estg.tourguide.services.LocationService;

public class WidgetRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {

    private Context context;
    private List<Place> list;
    private DataBaseHelper helper;
    private LocationService locationService;
    private boolean isServiceBound;

    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isServiceBound = false;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocationService.MyBinder myBinder = (LocationService.MyBinder) service;
            locationService = myBinder.getService();
            isServiceBound = true;
        }
    };

    public WidgetRemoteViewsFactory(Context context, Intent i) {
        this.context = context;
    }

    @Override
    public void onCreate() {
        helper = new DataBaseHelper(context);
        list = new ArrayList<>();
        Intent intent = new Intent(context, LocationService.class);
        context.bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
        getList(list);
    }

    @Override
    public void onDataSetChanged() {
        getList(list);
    }

    private void getList(List<Place> list) {
        list.clear();
        String type = PreferenceManager.getDefaultSharedPreferences(context)
                .getString("category_list", "");
        if (type != null) {
            if (type.equals("All")) {
                helper.getPlaces(list, null, false);
            } else {
                helper.searchByCategory(list, type, true);
            }
        } else {
            helper.getPlaces(list, null, false);
        }

        sortList(list);
        sizeList(list);
    }

    private void sizeList(List<Place> list) {
        String stringLastSize = PreferenceManager.getDefaultSharedPreferences(context)
                .getString("list_size", "10");

        int lastSize = 10;
        if (stringLastSize != null) {
            lastSize = Integer.valueOf(stringLastSize);
        }

        int size = list.size();
        if (size > lastSize) {
            Place[] newList = new Place[lastSize];
            for (int i = 0; i < lastSize; i++) {
                newList[i] = list.get(i);
            }
            list.clear();
            list.addAll(Arrays.asList(newList).subList(0, lastSize));
        }
    }

    private void sortList(List<Place> list) {
        String sortBy = PreferenceManager.getDefaultSharedPreferences(context)
                .getString("sort_list", "Rating");

        if (sortBy != null) {
            switch (sortBy) {
                case "Name A-Z":
                    Collections.sort(list, (o1, o2) ->
                            String.CASE_INSENSITIVE_ORDER.compare(o1.getNome(), o2.getNome()));
                    break;
                case "Name Z-A":
                    Collections.sort(list, (o1, o2) ->
                            String.CASE_INSENSITIVE_ORDER.compare(o1.getNome(), o2.getNome()));
                    Collections.reverse(list);
                    break;
                case "Proximity":
                    if (locationService != null) {
                        final LatLng coordinates = locationService.getCoordinates();
                        Place.closestPlaces(list, coordinates.latitude, coordinates.longitude);
                        radiusSort(list);
                    }
                    break;
                case "Rating 5-1":
                    Collections.sort(list, (o1, o2) -> {
                        int val1 = Integer.valueOf(o1.getRating());
                        int val2 = Integer.valueOf(o2.getRating());
                        return Integer.compare(val1, val2);
                    });
                    Collections.reverse(list);
                    break;
                default:
                    Collections.sort(list, (o1, o2) -> {
                        int val1 = Integer.valueOf(o1.getRating());
                        int val2 = Integer.valueOf(o2.getRating());
                        return Integer.compare(val1, val2);
                    });
                    break;
            }
        }
    }

    private void radiusSort(List<Place> list) {
        String prefRadius = PreferenceManager.getDefaultSharedPreferences(context)
                .getString("search_radius", "5");
        int radius = 5;
        if (prefRadius != null) {
            radius = Integer.valueOf(prefRadius);
        }

        int size = list.size();
        List<Place> placeHolder = new ArrayList<>(list);
        for (int i = 0; i < size; i++) {
            Place p = placeHolder.get(i);
            if (p._distance > radius) {
                list.remove(p);
            }
        }
    }

    @Override
    public void onDestroy() {
        list.clear();
        if (isServiceBound) {
            isServiceBound = false;
        }
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public RemoteViews getViewAt(int position) {
        RemoteViews row = new RemoteViews(context.getPackageName(), R.layout.widget_row);
        row.setTextViewText(R.id.item, list.get(position).getNome());

        Intent fillInIntent = new Intent();
        fillInIntent.putExtra("placePos", list.get(position).getId());
        fillInIntent.putExtra("fromWidget", true);

        row.setOnClickFillInIntent(R.id.item, fillInIntent);
        return row;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }
}
